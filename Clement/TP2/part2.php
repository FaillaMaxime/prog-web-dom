<?php

require_once("tp2-helpers.php");

//comptage("plussimple2.json");
function comptage ($namefile){
    $json = json_decode(file_get_contents($namefile));
    echo count($json->features) . "\n";
  }

creer_struct_CSV("plussimple.csv");
function creer_struct_CSV($namefile){
  $file = fopen($namefile,'r',);
  $i = 0;
  $tmp  = fgetcsv($file);
  while ( $tmp != null) {
    $tab[$i]["ant_id"] =$tmp[0];
    $tab[$i]["ad"] =$tmp[1];
    $tab[$i]["micro"] =$tmp[2];
    $tab[$i]["op"] =$tmp[3];
    $tab[$i]["ant_tech"] =$tmp[4];
    $tab[$i]["x"] =$tmp[5];
    $tab[$i]["y"] =$tmp[6];
    $tab[$i]["ant_ad"] =$tmp[7];
    $tab[$i]["num_cart"] =$tmp[8];
    $tab[$i]["num_supp"] =$tmp[9];
    $tab[$i]["2G"] =$tmp[10];
    $tab[$i]["3G"] =$tmp[11];
    $tab[$i]["4G"] =$tmp[12];
    $i++;
    $tmp  = fgetcsv($file);
  }
  fclose($file);
  //var_dump($tab);
  return $tab;
}