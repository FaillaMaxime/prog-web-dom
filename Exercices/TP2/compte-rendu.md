% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi :  TP2 Antennes

## Participants 

* Un
* Deux
* ...

1. En utilisant la commande *wc*, on dénombre 69 entrées dans le fichier. En analysant le fichier, on remarque que la première ligne ne décrit pas un point d'accès, mais indiqu le format du document. Le fichier contient donc 68 points d'accès.

2. On détermine le nombre de nombre de lieux uniques en filtrant le fichier par la colonne 3 ou 4 (la colonne 2 étant innutilisable à cause de la bibliothèque Kateb portant des noms différents malgré le fait que ce soit le même lieu). On utilise la colonne suivante : 
		*cat borneswifi.csv | cut -d , -f3 | uniq | wc*
Le lieu ayant le plus de bornes est la Bibliothèque Etude, avec 5 bornes. On l'obtient avec la commande suivante, puis en retrouvant le nom correspondant à la longitude observée.
		*cat borneswifi.csv | cut -d , -f3 | uniq -c | sort*
		
3. On importe le fichier dans un tableau, que l'on traverse ligne par ligne à l'aide d'un foreach. A chaque intération du foreach, on incrémente une variable de comptage. On affiche enfin le résultat en enlevant 1 valeur, l'en-tête du document.

4. On importe le fichier dans un tableau. On analyse chaque ligne en vérifiant d'abord si elle est nulle. Si ce n'est pas le cas, alors on affiche les valeurs des différents champs puis l'on passe à la ligne suivante.

5. On utilise la fonction distance() fournie et l'on calcule la distance entre le point précisé dans l'énoncé et chacun des points du fichier. On déclare 3 tableaux : le tableau contenant les informations du point précisé dans l'énoncé. Le tableau contenant la liste des lieux à moins de 200m de la cible. Le tableau contenant le lieu le plus proche de la cible.
Si la distance est inférieure à 200, alors on l'ajoute dans un tableau que l'on affichera en fin de programme. Si la distance est inférieure à la distance contenue dans le tableau $plus_proche, alors on remplace le précédent "lieu le plus proche trouvé" par l'élément actuel.
On dénombre 7 antennes proches, réparties dans 6 lieux différents. L'antenne la plus proche se situe dans la place Grenette.



    Partie 2 : Antenne GMS
* Un : Il y a 100 antennes. Comme informations supplémentaires nous avons l'opérateurs de l'antenne et nous savons si l'antenne supporte la 2G, 3G et 4G. C'est information peuvent être utiles car elles permettent de jaugé la couverture réseau offerte par les différents opérateurs ainsi que la qualité du réseau proposé.

* Deux : Il y a 4 oppérateur différents. "uniq -f 3 plussimple.csv | wc"
FREE = 18
SFR = 30
ORA = 26
BYG = 26

* Trois : Il semble difficile, d'après nos recherche, de valider un fichier kml. Il semblerai que passer par un validateur en ligne soit la manière la plus facile.

* Quatre : Il est n'est pas si difficile a lire en brut tel quel. Son usage étant limitée a certains logiciel, cela peut être contraignant. Néanmoins son utilisation n'est pas extremement compliqué. En terme de compacité, cela semble assez efficace. Il ne semble pas y avoir plus de redondance que dans les autres formats, ormis la structure de présentation qui se répète et dans le fait que chaque information est double puis que dans ecrite une fois puis répéter dans "Description".


