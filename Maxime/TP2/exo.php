<?php
include "../../Helpers/tp2-helpers.php";


//quesiton 3
//comptage("borneswifi_EPSG4326.json");
function comptage ($namefile){
  $json = json_decode(file_get_contents($namefile));
  echo count($json->features) . "\n";
}

//question 9
//creer_struct_JSON("borneswifi_EPSG4326.json");
function creer_struct_JSON($namefile){
  $json = json_decode(file_get_contents($namefile))->features;
  $i = 0;
  foreach ($json as $key => $value) {
    $tab[$i]["id"] = $value->properties->AP_ANTENNE1 ;
    $tab[$i]["lieu"] = $value->properties->{"Antenne 1"};
    $tab[$i]["lon"] = $value->geometry->coordinates[0];
    $tab[$i]["lat"] =$value->geometry->coordinates[1];
    $i++;
  }
  return $tab;
}


//question 4
//creer_struct_CSV("borneswifi_EPSG4326.csv");
function creer_struct_CSV($namefile){
  $file = fopen($namefile,'r',);
  $i = 0;
  $tmp  = fgetcsv($file);
  while ( $tmp != null){
    $tab[$i]["id"] =$tmp[0];
    $tab[$i]["lieu"] =$tmp[1];
    $tab[$i]["lon"] =$tmp[2];
    $tab[$i]["lat"] =$tmp[3];
    $i++;
    $tmp  = fgetcsv($file);
  }
  fclose($file);
  //var_dump($tab);
  return $tab;
}

//question 5
//affiche_distance_from("borneswifi_EPSG4326.csv" , 5.72752, 45.19102 );
function affiche_distance_from($namefile, $lon, $lat){
  $tab = creer_struct_CSV($namefile);
  $min = ['dist'=>9999999, 'elem' =>""];
  foreach ($tab as $key => $value) {
    $i = 0;
    $distance = distance($value,  geopoint($lon, $lat)) ;
    echo "from " . $lon . " , " . $lat . " to " . $value["lieu"] . " there is " . $distance . "m.\n";
    if($distance < 200){
      $liste_less_200[$i] = $value;
      $i++;
    }
    if($distance < $min["dist"]){
      $min["dist"] = $distance;
      $min["elem"] = $value;
    }
  }
  echo "\n\n less than 200 m away, there are : ";
  if (count($liste_less_200)==0){
    echo "no points\n";
  }else{
    echo count($liste_less_200) . " wifi points :\n";
    foreach ($liste_less_200 as $key => $value) {
      echo $value["lieu"] . "\n";
    }
  }
  echo "\n\nthe nearest one is : " . $min["elem"]["lieu"] ." " . $min["dist"] . "m away.\n";
}


//question 6
//shortest_N("borneswifi_EPSG4326.csv" ,5.72752, 45.19102, 5 );
function shortest_N ($namefile, $lon, $lat, $n){
  $tab = get_pos_and_dst_trie($namefile, $lon, $lat);
  echo "the ".$n." nearest are : \n";
  for($i=0;$i<$n;$i++){
    echo $tab[$i]["antene"]["lieu"] . " with ". $tab[$i]["dist"] . " m from you \n";
  }
}



//question 7
//ajout_addr($tab);
function ajout_addr($tab){
  foreach ($tab as $key => $value) {
    $tab[$key]["antene"]["addr"] = adress_of($value["antene"]["lon"], $value["antene"]["lat"]);
  }
  return ($tab);
}






//info_trie_distance("$tab" , 5.72752, 45.19102 );
function info_trie_distance($tab, $lon, $lat){
  $i = 0;
  foreach ($tab as $key => $value) {
    $pos_and_dist[$i] = ["dist"=>distance($value,  geopoint($lon, $lat)) , "antene"=>$value];
    $i ++;
  }
  $dist = array_column($pos_and_dist, 'dist');
  $antene = array_column($pos_and_dist, 'antene');
  array_multisort($dist,SORT_ASC,$antene,SORT_ASC,$pos_and_dist) ;
  return $pos_and_dist;
}



function adress_of($lon, $lat){
  $json = json_decode(smartcurl("https://api-adresse.data.gouv.fr/reverse/?lat=" . $lat. "&lon=" . $lon , 0) );
  return $json->features[0]->properties->name;
}


///////////////////////////////////////////////////
///////////////////////////////////////////////////
////                                           ////
////                  partie 2                 ////
////                                           ////
///////////////////////////////////////////////////
///////////////////////////////////////////////////


//GSM_data_SVG("DSPE_ANT_GSM_EPSG4326.csv");
function GSM_data_SVG($namefile){
  $file = fopen($namefile,'r',);
  $i = 0;
  $tmp  = fgetcsv($file, 1000, ";");
  while ($tmp != null){
    $tab[$i]["id"] =$tmp[1];
    $tab[$i]["operator"] = $tmp[3];
    $tab[$i]["lieu"] =$tmp[7];
    $tab[$i]["X"] =$tmp[5];
    $tab[$i]["Y"] =$tmp[6];
    $i++;
    $tmp  = fgetcsv($file, 1000, ";");
  }
  fclose($file);
  return $tab;
}

//question 1
//print_nb_antennes("DSPE_ANT_GSM_EPSG4326.csv");
function print_nb_antennes($namefile){
  echo "nombre d'antennes : " . count(GSM_data_SVG("DSPE_ANT_GSM_EPSG4326.csv")) . "\n";
}


//question 2 :
//data_operator("DSPE_ANT_GSM_EPSG4326.csv");
function data_operator($namefile){
  $tab = GSM_data_SVG("DSPE_ANT_GSM_EPSG4326.csv") ;


  foreach ($tab as $key => $value) {
    $res[$value["operator"]] = 0;
  }
  foreach ($tab as $key => $value) {
    $res[$value["operator"]] ++;
  }
  echo "il y as : " . count($res) . " opérateurs différents :\n";
  foreach ($res as $key => $value) {
    echo $key . " a " . $res[$key ] . " antènnes\n";
  }
}


//GSM_data_JSON("GSM_data.json");
function GSM_data_JSON($namefile){
  $json = json_decode(file_get_contents($namefile))->features;
  $i = 0;
  foreach ($json as $key => $value) {
    $tab[$i]["id"] =$value->properties->ANT_ID;
    $tab[$i]["operator"] = $value->properties->OPERATEUR;
    $tab[$i]["lieu"] = $value->properties->ANT_ADRES_LIBEL;
    $tab[$i]["X"] =$value->properties->X;
    $tab[$i]["Y"] =$value->properties->Y;
    $tab[$i]["lat"] =$value->geometry->coordinates[1];
    $tab[$i]["lon"] =$value->geometry->coordinates[0];
    $i++;
  }
  return $tab;
}



//info_trie_distance_operator(GSM_data_JSON("GSM_data.json"),5.72752,45.19102, "SFR");
function info_trie_distance_operator($tab, $lon, $lat,$op){
  $i = 0;
  foreach ($tab as $key => $value) {
    if($value["operator"] == $op){
      $pos_and_dist_and_op[$i] = ["dist"=>distance($value,  geopoint($lon, $lat)) , "antene"=>$value];
      $i ++;
    }
  }
  $dist = array_column($pos_and_dist_and_op, 'dist');
  $antene = array_column($pos_and_dist_and_op, 'antene');
  array_multisort($dist,SORT_ASC,$antene,SORT_ASC,$pos_and_dist_and_op) ;
  return $pos_and_dist_and_op;
}

/*
// trie sur les fournisseurs réseau

var_dump($tab); */

?>
