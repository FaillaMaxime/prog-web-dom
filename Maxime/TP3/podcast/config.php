<?php
require_once('vendor/dg/rss-php/src/Feed.php');
$url ="http://radiofrance-podcast.net/podcast09/rss_14312.xml";
$rss = Feed::loadRss($url);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Podcasts</title>
	</head>
	<body>
		<?php
		echo "<h1>".$rss->title."</h1>";
		 ?>

		<table>
		<thead>
		<tr>
		  <th>Podcast</th>
		  <th>Date</th>
		  <th>Titres</th>
		  <th>Play</th>
		  <th>Durée</th>
		  <th>téléchargement</th>
		</tr>
		</thead>
		<tbody>
		<?php

		$i = 1;
		foreach ($rss->item as $item) {
			echo "<tr>";
			echo "<th>$i</th>" ;
			echo "<td>$item->pubDate</td>";
			echo "<td> <a title=\"$item->description\" href=\"$item->link\">$item->title</a> </td>";
			echo "<td> <audio controls src=\"". $item->enclosure["url"]."\"> Your browser does not support the <code>audio</code> element.</audio> </td>";
			echo "<td>".$item->{"itunes:duration"} ."</td>" ;
			echo "<td> <a href=\"".$item->enclosure["url"]." \" download=\"".$item->enclosure["url"]."\">download</a></td>";
			echo "</tr>";
			$i++;
		}

		?>

		</tbody>
		</table>
	</body>
</html>
