<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <?php
    // affiche le nom de l'acteur en tant que nom d'onglet si passé en paramètre
    // le passage en paramètre est nécessaire car les balises php sont séparer, donc cela demanderais 2 apelles api pour avoir l'information aux 2 endroit
    // la fusion des 2 balises php ne semble pas etre une bonne idée car une est utilisé pour l'entete alors que l'autre est pour l'affichage du coeur de la page

    // sécirité si le nom n'es pas passez en paramètre meme si cela ne devrais pas arrivé a partir de la page de présentation de film
    if(isset($_GET["nom_acteur"])){
      echo "<title>".$_GET["nom_acteur"]."</title>";
    }else{
      echo "<title>Detaille d'acteur</title>";
    }
    ?>
  </head>
  <body>
    <?php
      include "tp3-helpers.php";

      // sécurité sur l'identifiant de l'acteur meme si cela ne devrais pas arrivé a partir de la page de présentation de film
      $id_recherche_acteur = 20;
      if(isset($_GET["id_recherche_acteur"])){
        $id_recherche_acteur= $_GET["id_recherche_acteur"];
      }

      // sécurité sur la cle de l'API meme si cela ne devrais pas arrivé a partir de la page de présentation de film
      $api_key = "ebb02613ce5a2ae58fde00f4db95a9c1";
      if(isset($_GET["api_key"])){
        $api_key= $_GET["api_key"];
      }


      echo "<h1>Question poussée</h1>";
      echo "<h3>question 9</h3>";

      // affichage des information de l'acteur
      $acteur_liste_film = get_actor_list_film_from_id($id_recherche_acteur);
      affiche_film_from_acteur($acteur_liste_film);


      ////////////////////////////
      /////// fonctions //////////
      ////////////////////////////

      /*
      USE : global $api_key
      RETURN : la liste des films auquels l'acteur en paramètre à participé
              pour chaque film des informations le concernant sont fournie
      */
      function get_actor_list_film_from_id($id){
        global $api_key;
        $reponse_curl = smartcurl("https://api.themoviedb.org/3/person/$id/combined_credits?api_key=$api_key&language=en-US")[0];
        return json_decode($reponse_curl)->cast;
      }

      //////////////////////////////////
      ////// FONCTION D'AFFICHAGE //////
      //////////////////////////////////

      /*
      affichage de tous les film auquels l'acteur a participé ainsi que le role incarné
        format d'affichage : Nom du film , Role joué
      */
      function affiche_film_from_acteur($acteur_liste_film){
        echo "<table>
        <thead>
        <tr>
          <th>Nom du film</th>
          <th>Role</th>
        </tr>
        </thead>
        <tbody>" ;
        foreach ($acteur_liste_film as $key => $film) {
          if($film->media_type == "movie"){
            echo"<tr><td>$film->title</td><td>$film->character</td></tr>";
          }
        }
        echo "</tbody></table>";
      }

      //fonction de débugage pour afficher les variables avec le var_dump dans un format plus lisible
      function affichage_variable($var){
        echo '<pre>'; var_dump($var); echo '</pre>';
      }
     ?>
  </body>
</html>
