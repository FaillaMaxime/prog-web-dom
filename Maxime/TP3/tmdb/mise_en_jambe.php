<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>TMDB</title>
  </head>
  <body>
    <h1>Mise en jambe TMDB</h1>
    <h3>question 1</h3>
    <p>Le format de retour de l'api est le JSON </p>
    <p>Le fime dont il est question est : Fight Club</p>
    <p>Avec le paramère supplémentaire "language=fr" les éléments sont donnée en francais (mais pas les clefs)</p>

    <h3>question 3</h3>
    <p>entrez l'id du film si dessous et clicker sur le boutton, par défaut l'id est 550 pour le film Fight club</p>

    <form method="get" action="mise_en_jambe.php">
      <label for="id_recherche_film">id :</label> <input type="text" id="id_recherche_film" name="id_recherche_film"/>
      <input type="submit"/>
    </form>

    <?php
      include "tp3-helpers.php";
      // variables globals
      // elles peuvent etre utilisé dans dans des fonctions comme la clef de l'API pour les requettes. cela permet une grande flexibilité pour la communicaiton avec cette dernière
      $id_recherche_film = 550;
      if (isset($_GET["id_recherche_film"])){ // permet de toujours afficher le détaille d'un film, meme si il n'y as pas encore eu de requette
        $id_recherche_film =$_GET["id_recherche_film"];
      }
      $language_to_extension = array("Anglais"=>"&language=en" , "Francais"=>"&language=fr", "Version original"=>""); // variable blobale associant à un language, l'extention a rajouté à une requette affin d'avoir un resultat dans la bonne langue
      $api_key = "ebb02613ce5a2ae58fde00f4db95a9c1";


      // recuperaiton des informations du film en Francais
      // stockage dans une liste associative afin de pouvoir avoir un affichage de manière très simple avec autant d'éléméent que l'on souhaite
      $films["Francais"] = get_film_from_id_and_language($id_recherche_film, "Francais");
      affichage_info_film($films["Francais"],"Français");

      echo "<h1>Question poussée </h1>";

      echo "<h3>question 4 & 5 & 10</h3>";

      // ajout de deux nouvelles langues dans le talbeau associatif est affichage
      $films["Version original"] = get_film_from_id_and_language($id_recherche_film, "Version original");
      $films["Anglais"] = get_film_from_id_and_language($id_recherche_film, "Anglais");
      affichage_info_films_multi_langues_avec_poster($films);


      echo "<h3>question 6</h3>";
      echo"<p> films de TLOTR (The Lord Of The Ring) trouvé sur the mouvie db</p>";
      // cette variable permet de pouvoir afficher un liste d'un autre type de film avec une grande simplicité
      $name_film = "The+Lord+of+the+Rings";

      $collection_liste_TLORT = get_collection_liste_from_name($name_film);
      affichage_collection_TLOTR($collection_liste_TLORT);

      echo "<h3>question 7 & 9</h3>";

      affichage_acteur_from_collection_liste($collection_liste_TLORT);

      echo "<h3>question 8</h3>";
      echo "<p>Il n'y as aucun moyen (tout du moins nous n'en avons pas trouver) de connaitre la race d'un personnage a patir de l'API. la seul solution avec uniquement cette API serais de prédéterminé la liste de tous les personnages de la race Hobbit et, de vérifié pour chaque acteur si le personnage incarnée est un hobbit ou non</p>";
      echo "<p>Une autre idée pourrais être d'accédé à une autre existante qui contient le nom des des personnages et leur race, de cette manière nous pourions, en effectuant des appelles a cette API, savoir quels sont les personnages de la race Hobbit";


      ////////////////////////////
      /////// fonctions //////////
      ////////////////////////////

      /*
      fonciton pour la question 1
      fonction de prise en main de l'appelle à l'API avec tmbdget
      */
      function exploration (){
        $api_result_without_param = tmdbget("movie/550");
        return json_decode(tmdbget("movie/550", array ("language=fr") ));
      }

      /*
      fonction pour la question 2
      USE : global api_key
      fonction de prise en main de l'appelle à l'API avec smartcurl (format utilsé pour les fonctions suivantes)
      */
      function exploration_cli (){
        global $api_key ;
        $reponse_curl = smartcurl("http://api.themoviedb.org/3/movie/550?api_key=global$api_key&language=fr");
        return json_decode($reponse_curl[0]);
      }

      /*
      fonction pour la question 3
      USE : global $api_key , $language_to_extension
      RETURN : données d'un film en fonction de son identifiant et de la langue souhaité
      */
      function get_film_from_id_and_language($id, $language){
        global $language_to_extension;
        global $api_key ;
        $reponse_curl = smartcurl("http://api.themoviedb.org/3/movie/$id?api_key=$api_key". $language_to_extension[$language])[0];
        return json_decode($reponse_curl);
      }

      /*
      USE : global $api_key
      RETURN : la liste de film a partir de l'identifiant d'UNE collection
      */
      function get_film_liste_from_id_collection($id){
        global $api_key;
        $reponse_curl = smartcurl("https://api.themoviedb.org/3/collection/$id?api_key=$api_key")[0];
        return json_decode($reponse_curl);
      }

      /*
      fonction pour la question 6
      USE : global $api_key
      RETURN : une liste de collection de film a patir d'un nom (doit etre déjà encodé)
      */
      function get_collection_liste_from_name($name){
        global $api_key;
        $reponse_curl = smartcurl("http://api.themoviedb.org/3/search/collection?api_key=$api_key&query=$name")[0];
        return json_decode($reponse_curl)->results;
      }

      /*
      USE : global $api_key
      RETURN : credits (liste acteur entre autre) d'un film en fonction de son identifiant
      */
      function get_credit_film_from_id($id){
        global $api_key;
        $reponse_curl = smartcurl("http://api.themoviedb.org/3/movie/$id/credits?api_key=$api_key")[0];
        return json_decode($reponse_curl);
      }

      /*
      CALL : get_credit_film_from_id()
      RETURN : une liste de tout les acteurs d'une liste de film
      associe à chaque acteur les information donnée par l'API ainsi que le nombre de film dans lesquels il as participé (indépendant du nombre de role qu'il a jouer dans tout les films)
      présume que l'acteur n'as eu que un role par film et qu'il soit le meme pour tous les films de la liste. sinon seul un seul role sera associé à l'acteur
      */
      function get_actor_from_film_liste ($liste_film){
        $liste_acteur = null;
        foreach ($liste_film as $key => $film) {
          $cast = get_credit_film_from_id($film->id)->cast;
          foreach ($cast as $key => $actor) {
            if( ! isset($liste_acteur[$actor->id])){
              $liste_acteur[$actor->id] = $actor;
              $liste_acteur[$actor->id]->nb_film_present = 1;
            }else{
              $liste_acteur[$actor->id]->nb_film_present ++;
            }
          }
        }
        return $liste_acteur;
      }

      /*
      USE : global $api_key
      RETURN : informations vidéo d'un film en fonction de son id et de la langue souhaité
      null si le film n'as pas de vidéo associé
      */
      function get_trailer_from_film_id($id, $extention){
        global $api_key;
        $reponse_curl = smartcurl("https://api.themoviedb.org/3/movie/$id/videos?api_key=$api_key$extention")[0];
        $decode =json_decode($reponse_curl);
        if(isset($decode->results[0])){
          return $decode->results[0];
        }else{
          return null ;
        }
      }

      //////////////////////////////////
      ////// FONCTION D'AFFICHAGE //////
      //////////////////////////////////

      // affichage de l'entete du tableau de présentation de films (un film par ligne)
      function affiche_entete_tableau(){
        echo "<table>
        <thead>
        <tr>
        <th>Version</th>
        <th>Titre</th>
        <th>Titre original</th>
        <th>Tagline (si existe)</th>
        <th>Description</th>
        <th>Lien vers le film</th>
        </tr>
        </thead>
        <tbody>" ;
      }

      //affichage de la fin du tableau de présentation de films (un film par ligne)
      function affiche_fin_tableau(){
        echo "</tr></tbody></table>";
      }

      /*
      CALL : affiche_entete_tableau(),  affiche_fin_tableau()
      affichage des informations d'un film dans un tableau en ligne (facilment adaptable à l'affichage de plusieurs films)
      */
      function affichage_info_film($film, $version){
        affiche_entete_tableau();
        echo "<tr>";
        echo "<th>$version</th>";
        echo "<td>$film->title </td>";
        echo "<td>$film->original_title </td>";
        if(isset($film->tagline)){
          echo "<td>$film->tagline </td>";
        }else{
          echo "<td>Sa tagline n'existe pas</td>";
        }
        echo "<td>$film->overview </td>";
        echo "<td> <a href=https://www.themoviedb.org/movie/$film->id>https://www.themoviedb.org/movie/$film->id</a> </td></tr>";
        affiche_fin_tableau();
      }

      /*
      USE : global $language_to_extension
      CALL : get_trailer_from_film_id()
      affichage des information de chacun de la liste de film en paramètre en collonne
      information afficher dans cette ordre : Version , Titre , TagLine , Desciption , lien vers le film, Poster, Trailer
      la Tagline comme le trailler peuvent ne pas exister et donc ne serons pas affiché
      */
      function affichage_info_films_multi_langues_avec_poster($films){
        global $language_to_extension ;
        echo"<table> <tr><th>version</th> ";
        foreach ($films as $language => $film) {
          echo "<td>$language</td>";
        }
        echo "</tr><tr><th>Titre</th>";
        foreach ($films as $language => $film) {
          echo "<td>$film->title</td>";
        }
        echo "</tr><tr><th>TagLine</th>";
        foreach ($films as $language => $film) {
          if(isset($film->tagline)){ // si la tagline existe pour ce film
            echo "<td>$film->tagline </td>";
          }else{
            echo "<td>Sa tagline n'existe pas</td>";
          }
        }
        echo "</tr><tr><th>Description</th>";
        foreach ($films as $language => $film) {
          echo "<td>$film->overview</td>";
        }
        echo "</tr><tr><th>Lien vers le film</th>";
        foreach ($films as $language => $film) { // affichage du lien vers le film
          echo "<td><a href=https://www.themoviedb.org/movie/$film->id$language_to_extension[$language]>https://www.themoviedb.org/movie/$film->id$language_to_extension[$language]</a></td>";
        }
        //partie question 5
        echo "</tr><tr><th>Poster</th>";
        foreach ($films as $language => $film) { // affichage du poster du film
          echo "<td> <img src=\"http://image.tmdb.org/t/p/w92/$film->poster_path\"> </td>";
        }
        //partie question 10
        echo "</tr><tr><th>Trailler</th>";
        foreach ($films as $language => $film) { // affichage de la vidéo du trailler, fonctionne actuellement uniquement si le site est youtube
          $trailer = get_trailer_from_film_id($film->id, $language_to_extension[$language]);
          if(isset($trailer) && $trailer->site == "YouTube"){
            echo"<td><iframe width=\"480\" height=\"270\" src=\"https://www.youtube.com/embed/$trailer->key\"
              title=\"Teaser\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe></td>";
            }else{
              echo "<td>plateforme d'ébergement non pris en compte ou trailler indisponible</td>";
            }
          }
          echo"</tr></table>";
        }

      /*
        CALL : affichage_info_liste_film_TLOTR() , get_film_liste_from_id_collection()
        fonction d'affichage de d'une liste de collection
        */
      function affichage_collection_TLOTR($collection_liste){
          $i = 1;
          foreach ($collection_liste as $key => $collection) {
            echo "<p> collection n°$i :</p>";
            affichage_info_liste_film_TLOTR( get_film_liste_from_id_collection($collection->id)->parts);
            $i++;
          }
        }

      /*
        fonction d'affichage d'information d'une liste de film au format TLOTR
        le format TLOTR correspond à : Identifiant, Date de sortie, Titre
        */
      function affichage_info_liste_film_TLOTR($films){
          echo "<table><thead><tr>
          <th>Identifiant</th>
          <th>Date de sortie</th>
          <th>Titre</th>
          </tr></thead><tbody>" ;
          foreach ($films as $key => $film) {
            echo "<tr><td>$film->id</td><td>$film->release_date</td><td>$film->title</td></tr>";
          }
          echo "</tbody></table>";
        }

      /*
        CALL : get_film_liste_from_id_collection(), get_actor_from_film_liste()
        USE : global $api_key
        affichage de tous les ACTEURS des fims d'une liste de collection     LES FILMS ET COLLECTION NE SONT PAS AFFICHER
        liste d'acteur séparer par collection
        format d'affichage : Nom , Role , Nombre d'apparition dans cette collection
        Le nom est un hyperlien menant à une page de présentation de l'acteur (cf acteur.php)
        */
      function affichage_acteur_from_collection_liste($collection_liste){
          global $api_key;
          $i = 1;
          foreach ($collection_liste as $key => $collection) {
            echo "<p>Liste des acteurs de la collection n°$i :</p>";
            $liste_film = get_film_liste_from_id_collection($collection->id)->parts;
            $liste_acteur = get_actor_from_film_liste($liste_film);
            echo "<table><thead><tr>
            <td>Nom</td>
            <td>Role</td>
            <td>nombre de film présent dans la collection</td>
            </tr></thead><tbody>";
            foreach ($liste_acteur as $key => $acteur) {
              $lien = "<a href=\"acteur.php?api_key=$api_key&id_recherche_acteur=$acteur->id&nom_acteur=$acteur->name\" target=\"_blank\" >$acteur->name</a> "; // creation de la balise menant vers acteur.php
              echo "<tr><td>$lien</td><td>$acteur->character</td><td>$acteur->nb_film_present</td></tr>";
            }
            echo "</tbody></table>";
            $i++;
          }
        }

      //fonction de débugage pour afficher les variables avec le var_dump dans un format plus lisible
      function affichage_variable($var){
        echo '<pre>'; var_dump($var); echo '</pre>';
      }

    ?>
  </body>
</html>
