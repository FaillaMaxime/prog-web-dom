<?php
require_once("tp2-helpers.php");

$file = fopen('borneswifi_EPSG4326_20171004.csv', 'r');
$data = fgetcsv( $file, ",");
$lat=45.19102;
$lon=5.72752;
$Grenette = geopoint($lon, $lat);
$lieux_proche = array();
$plus_proche = array("",200);


while (($data = fgetcsv( $file, ",")) != Null) {
	$lieu = geopoint($data[2], $data[3]);
	$dist = distance($lieu, $Grenette);
	if ($dist <= 200 and $dist !=0)
		array_push($lieux_proche, $data[1]);
		
	if ($dist < $plus_proche[1]) {
		$plus_proche[0] = $data[1];
		$plus_proche[1] = $dist;
		}

}

echo "lieux proche de la place Grenette : \n";

foreach ($lieux_proche as $line) {
	echo $line;
	echo "\n";
	}

echo "Le lieu le plus proche est : \n";
echo $plus_proche[0];
echo "\n";
