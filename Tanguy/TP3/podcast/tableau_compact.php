<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="zer.css">
</head>
<body>



<table>

<thead>
<th> Semaine </th>
<th colspan="2"> Vendredi </th>
<th colspan="2"> Jeudi </th>
<th colspan="2"> Mercredi </th>
<th colspan="2"> Mardi </th>
<th colspan="2"> Lundi </th>
</thead>



<?php
// préparation du flux rss
require_once('vendor/dg/rss-php/src/Feed.php');

$url = "http://radiofrance-podcast.net/podcast09/rss_14312.xml";

$rss = Feed::loadRss($url);

$url2 = "http://radiofrance-podcast.net/podcast09/rss_14311.xml";

$rss2 = Feed::loadRss($url2);

?>

<?php

//on utilise le titre du flux rss pour le titre de l'onglet
echo  '<title>',$rss->title,'</title>';



// tableau contenant les valeurs des semaine que l'on a déjà rencontré.
$semaines_passées = array();
$années_passées = array();


// parcours de tous les objects contenu dans le flux
foreach ($rss->item as $item) {
	
	// on transforme la date dans le format int afin de pouvoir utiliser la fonction date()
	// ensuite, on test si la valeur obtenue a déjà été rencontrée
	$date_int = strtotime($item->pubDate);
	$semaine = date("W",$date_int);
	$année = date("Y",$date_int);
	
	
	foreach ($rss2->item as $item2) {
		$date_int2 = strtotime($item2->pubDate);
		$semaine2 = date("W",$date_int2);
		$année2 = date("Y",$date_int2);
		
		// on vérifie la date de l'autre flux. On affiche l'élément le plus ancien des 2 flux
		if ($date_int2 > $date_int) {
			$desc = explode(" - ",$item2->description);
	
	
			// dans le cas où l'on change de semaine (on regarde le dernier élément du tableau et on le compare à l'élément en cours)
			if (end($semaines_passées) != $semaine2 || end($années_passées) != $année2) {
				//on change le dernier élément et on change de ligne
				array_push($semaines_passées,$semaine2);
				array_push($années_passées,$année2);
				echo '<tr> <td align="center"> année : ',$année2,'<br> semaine : ', $semaine2, '</td>';
			}
	
	
			// ajout des informations de l'élément
			echo '<td align="center"><a href = ',$item2->link,' title="',$desc[3],'">', $item2->title,'</a>';
			echo '<br>', substr($item2->pubDate, 0,17);
			echo '<br> <audio controls src="',$item2->enclosure["url"],'">';

			
			//cette ligne permettant de passer à l'élément suivant est atteinte uniquement si l'élément a été affiché. Si l'élément n'as pas respecté la condition de date, alors il sera à nouveau testé au prochain élément du flux 1.
			$item2->{'content:encoded'};
		
		}
	
	}
	
	
	
	// permet de séparer les informations superflues de la description, on utilise le 3 ieme champ de $desc qui contient la vraie description
	$desc = explode(" - ",$item->description);
	
	
	// dans le cas où l'on change de semaine (on regarde le dernier élément du tableau et on le compare à l'élément en cours)
	if (end($semaines_passées) != $semaine || end($années_passées) != $année) {
	//on change le dernier élément et on change de ligne
	array_push($semaines_passées,$semaine);
	array_push($années_passées,$année);
	echo '<tr> <td align="center"> année : ',$année,' <br> semaine : ', $semaine, '</td>';
	}
	
	
	// ajout des informations de l'élément
	echo '<td align="center"><a href = ',$item->link,' title="',$desc[3],'">', $item->title,'</a>';
	echo '<br>', substr($item->pubDate, 0,17);
	echo '<br> <audio controls src="',$item->enclosure["url"],'">';

	
	$item->{'content:encoded'};
}
  
  
  
  // le second flux ne suit pas le même rythme de diffusion que "la méthode scientifique", cela a pour effet de bord de totalement déborder des limites initiales du tableau.

  ?>


</body>
</html>


