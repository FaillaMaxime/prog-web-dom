<?php
require_once("tp3-helpers.php");
$id_film= 530; 
$ressource = tmdbget("movie/".$id_film, ['language' => 'fr']);

//var_dump($ressource);

// le fichier que l'on vas utiliser
$json = json_decode($ressource);



echo "Le nom du film est : "; 
print $json->{'title'}; 
echo "<br><br>";

if ($json->{'original_title'} != $json->{'title'}) {
echo "Le nom original du film est : ";
print $json->{'original_title'}; 
echo "<br><br>";
}

if ($json->{'tagline'} != null) {
echo "Le tag du film est : ";
print $json->{'tagline'}; 
echo "<br><br>";
}


echo "La synopsis est : <br>";
print $json->{'overview'}; 
echo "<br><br>";


$lien_film = "https://www.themoviedb.org/movie/".$json->{'id'};
echo "La page du film est consultable <a href=$lien_film>ici</a>." ;


//var_dump($json);

?>

</body>
</html>
